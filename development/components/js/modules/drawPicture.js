module.exports = class AddDrawPicture {
	constructor(picTarget, picUrl, picRepeat) {
		this.picTarget = picTarget;
		this.picUrl = picUrl;
		this.picRepeat = picRepeat;
	}

	static info() {
		console.log('MODULE:', this.name, true);
	}

	run() {
		// const elem = this.picUrl;
		// if (elem.inedxOf() === -1) return;
		this.constructor.info();
		console.log('checked');
		const canvas = document.querySelector(`${this.picTarget}`);
		const ctx = canvas.getContext('2d');

		const picture = new Image();
		picture.src = this.picUrl;

		picture.onload = () => {
				const x = 0;
				const u = picture.width;
				const i = picture.height;
				ctx.imageSmoothingEnabled = false;
				ctx.drawImage(picture, x, x, u, i, x, x, u, i, this.picRepeat);
			};
	}
};
